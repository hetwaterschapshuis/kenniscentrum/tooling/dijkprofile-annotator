joblib==1.1.0
matplotlib==3.5.0
numpy==1.21.4
Pillow==8.4.0
scikit_learn==1.0.1
seaborn==0.11.2
torch==1.10.0